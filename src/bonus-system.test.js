import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");
const standardProgram = "Standard";
const premiumProgram = "Premium";
const diamondProgram = "Diamond";

describe('Calculate bonuses tests', () => 
{
    console.log("Tests started");

    test('Nonsense program and [1, 10000) amount', (done) =>
    {
        let minValue = 1;
        let amount = 1 + Math.random() * 9998;
        let program = "Nonsense" + Math.random().toString();
        expect(calculateBonuses(program, amount)).toEqual(0);
        expect(calculateBonuses(program, minValue)).toEqual(0);
        done();
    });

    test('Standard program and [1, 10000) amount', (done) =>
    {
        let minValue = 1;
        let amount = 1 + Math.random() * 9998;
        let program = standardProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.05);
        expect(calculateBonuses(program, minValue)).toEqual(0.05);
        done();
    })

    test('Premium program and [1, 10000) amount', (done) =>
    {
        let minValue = 1;
        let amount = 1 + Math.random() * 9998;
        let program = premiumProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.1);
        expect(calculateBonuses(program, minValue)).toEqual(0.1);
        done();
    })

    test('Diamond program and [1, 10000) amount', (done) =>
    {
        let minValue = 1;
        let amount = 1 + Math.random() * 9998;
        let program = diamondProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.2);
        expect(calculateBonuses(program, minValue)).toEqual(0.2);
        done();
    })

    test('Nonsense program and [10000, 50000) amount', (done) =>
    {
        let minValue = 10000;
        let amount = 10000 + Math.random() * 39999;
        let program = "Nonsense" + Math.random().toString();
        expect(calculateBonuses(program, amount)).toEqual(0);
        expect(calculateBonuses(program, minValue)).toEqual(0);
        done();
    });

    test('Standard program and [10000, 50000) amount', (done) =>
    {
        let minValue = 10000;
        let amount = 10000 + Math.random() * 39999;
        let program = standardProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.05*1.5);
        expect(calculateBonuses(program, minValue)).toEqual(0.05*1.5);
        done();
    })

    test('Premium program and [10000, 50000 amount', (done) =>
    {
        let minValue = 10000;
        let amount = 10000 + Math.random() * 39999;
        let program = premiumProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.1*1.5);
        expect(calculateBonuses(program, minValue)).toEqual(0.1*1.5);
        done();
    })

    test('Diamond program and [10000, 50000) amount', (done) =>
    {
        let minValue = 10000;
        let amount = 10000 + Math.random() * 39999;
        let program = diamondProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.2*1.5);
        expect(calculateBonuses(program, minValue)).toEqual(0.2*1.5);
        done();
    })

    test('Nonsense program and [50000, 100000) amount', (done) =>
    {
        let minValue = 50000;
        let amount = 50000 + Math.random() * 49999;
        let program = "Nonsense" + Math.random().toString();
        expect(calculateBonuses(program, amount)).toEqual(0);
        expect(calculateBonuses(program, minValue)).toEqual(0);
        done();
    });

    test('Standard program and [50000, 100000) amount', (done) =>
    {
        let minValue = 50000;
        let amount = 50000 + Math.random() * 49999;
        let program = standardProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.05*2);
        expect(calculateBonuses(program, minValue)).toEqual(0.05*2);
        done();
    })

    test('Premium program and [50000, 100000) amount', (done) =>
    {
        let minValue = 50000;
        let amount = 50000 + Math.random() * 49999;
        let program = premiumProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.1*2);
        expect(calculateBonuses(program, minValue)).toEqual(0.1*2);
        done();
    })

    test('Diamond program and [50000, 100000) amount', (done) =>
    {
        let minValue = 50000;
        let amount = 50000 + Math.random() * 49999;
        let program = diamondProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.2*2);
        expect(calculateBonuses(program, minValue)).toEqual(0.2*2);
        done();
    })

    test('Nonsense program and 100000+ amount', (done) =>
    {
        let minValue = 100000;
        let amount = 100000 + Math.random() * (Number.MAX_SAFE_INTEGER-100001);
        let program = "Nonsense" + Math.random().toString();
        expect(calculateBonuses(program, amount)).toEqual(0);
        expect(calculateBonuses(program, minValue)).toEqual(0);
        done();
    });

    test('Standard program and 100000+ amount', (done) =>
    {
        let minValue = 100000;
        let amount = 100000 + Math.random() * (Number.MAX_SAFE_INTEGER-100001);
        let program = standardProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.05*2.5);
        expect(calculateBonuses(program, minValue)).toEqual(0.05*2.5);
        done();
    })

    test('Premium program and 100000+ amount', (done) =>
    {
        let minValue = 100000;
        let amount = 100000 + Math.random() * (Number.MAX_SAFE_INTEGER-100001);
        let program = premiumProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.1*2.5);
        expect(calculateBonuses(program, minValue)).toEqual(0.1*2.5);
        done();
    })

    test('Diamond program and 100000+ amount', (done) =>
    {
        let minValue = 100000;
        let amount = 100000 + Math.random() * (Number.MAX_SAFE_INTEGER-100001);
        let program = diamondProgram;
        expect(calculateBonuses(program, amount)).toEqual(0.2*2.5);
        expect(calculateBonuses(program, minValue)).toEqual(0.2*2.5);
        done();
    })
});